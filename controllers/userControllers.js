const User = require("../models/User.js");

module.exports.createUserControllers = (req, res) => {
    User.findOne({ username: req.body.username })
        .then((result) => {
            if (result !== null && result.username === req.body.username) {
                return res.send("Username already exists!");
            } else {
                let newUser = new User({
                    username: req.body.username,
                    password: req.body.password,
                });
                newUser
                    .save()
                    .then((result) => res.send(result))
                    .catch((error) => res.send(error));
            }
        })
        .catch((error) => res.send(error));
};

// Retrieve ALL USers

module.exports.getAllUserController = (req, res) => {
    User.find({})
        .then((result) => res.send(result))
        .catch((error) => res.send(error));
};