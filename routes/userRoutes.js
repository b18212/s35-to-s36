const express = require("express");
const router = express.Router();

const userControllers = require("../controllers/userControllers");

console.log(userControllers);

router.post("/", userControllers.createUserController);

router.get("/", userControllers.getAllUserController);

module.exports = router;